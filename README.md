<h1 align="center">
  <br>
  Phaser 3 TypeScript Game
  <br>
</h1>


## How To Use

To run this game:


# Go into the repository
$ cd side-scroller-shooter-game

# Install dependencies
$ npm install

# Start the local development server (on port 8080)
$ npm start

# Build the production ready code to the /dist folder
$ npm run build

# Play your production ready game in the browser
$ npm run serve
