import { GameScene } from '../scenes/GameScene';
import { Enemy } from './Enemy';
import { Fires } from './Fires';

export class Enemies extends Phaser.Physics.Arcade.Group {
  private _timer: Phaser.Time.TimerEvent;
  private _countMax = 3;
  private _countCreated = 0;
  private _countKilled = 0;
  public fires: Fires;

  constructor(private _scene: GameScene, count?: number) {
    super(_scene.physics.world, _scene);
    this._timer = _scene.time.addEvent({
      delay: 1000,
      callback: this._onTimerTick,
      callbackScope: this,
      loop: true,
    });
    this.fires = new Fires(this._scene);
    if (count) {
      this._countMax = count + 2;
    }
  }

  private _onEnemyKilled(): void {
    ++this._countKilled;

    if (this._countKilled >= this._countMax) {
      this.scene.events.emit('enemies-killed');
    }
  }

  private _createEnemy(): void {
    let enemy = this.getFirstDead();
    if (!enemy) {
      enemy = Enemy.generate(this._scene, this.fires);
      enemy.on('killed', this._onEnemyKilled, this);
      this.add(enemy);
    } else {
      enemy.reset();
    }

    enemy.move();
    ++this._countCreated;
  }

  private _onTimerTick() {
    if (this._countCreated < this._countMax) {
      this._createEnemy();
    } else {
      this._timer.remove();
    }
  }
}
