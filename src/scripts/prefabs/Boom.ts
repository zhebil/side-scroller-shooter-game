import { GameScene } from '../scenes/GameScene';

export interface boomData {
  scene: GameScene;
  x: number;
  y: number;
}

export class Boom extends Phaser.GameObjects.Sprite {
  static generate(scene: GameScene, x: number, y: number): Boom {
    return new Boom({ scene, x, y });
  }
  constructor(data: boomData) {
    super(data.scene, data.x, data.y, 'boom', 'boom1');
    this.scene.add.existing(this);

    const frames = data.scene.anims.generateFrameNames('boom', {
      prefix: 'boom',
      start: 1,
      end: 4,
    });

    data.scene.anims.create({
      key: 'boom',
      frames,
      frameRate: 10,
      repeat: 0,
    });

    this.play('boom');
    this.once(Phaser.Animations.Events.ANIMATION_COMPLETE, () => {
      this.destroy();
    });
  }
}
