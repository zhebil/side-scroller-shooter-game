import { WIDTH } from '../const';
import { GameScene } from '../scenes/GameScene';
import { MovableObject, movableConstructorData } from './MovableObject';
import { Player } from './Player';

export class Fire extends MovableObject {
  public scene: GameScene;
  public body: Phaser.Physics.Arcade.Body;

  static generate(scene: GameScene, source: Player): Fire {
    const data: movableConstructorData = {
      scene,
      x: source.x,
      y: source.y,
      texture: source.bullet.texture,
      velocity: source.bullet.velocity,
    };
    return new Fire(data);
  }

  public isDead(): boolean {
    return this.x < +this.width || this.x > WIDTH + this.width;
  }
}
