import { HEIGHT, WIDTH } from '../const';
import { GameScene } from '../scenes/GameScene';
import { Fires } from './Fires';
import { movableConstructorData, MovableObject } from './MovableObject';

export interface attributes {
  x: number;
  y: number;
  frame: string;
}

export interface bullet {
  delay: number;
  texture: string;
  velocity: number;
}

export class Enemy extends MovableObject {
  public scene: GameScene;
  public body: Phaser.Physics.Arcade.Body;
  public fires: Fires;
  public timer: Phaser.Time.TimerEvent;
  public bullet: bullet;
  static generateAttributes(): attributes {
    const x = WIDTH + 150;
    // eslint-disable-next-line
    const y = Phaser.Math.Between(100, HEIGHT - 100);
    // eslint-disable-next-line
    const id = Phaser.Math.Between(1, 4);
    return { x, y, frame: `enemy${id}` };
  }

  static generate(scene: GameScene, fires: Fires): Enemy {
    const data = Enemy.generateAttributes();

    return new Enemy({
      scene,
      texture: 'enemy',
      velocity: -250,
      ...data,
      bullet: { delay: 1000, texture: 'bullet', velocity: -500 },
      origin: { x: 0, y: 0.5 },
      fires,
    });
  }
  public init(data: movableConstructorData): void {
    super.init(data);
    this.setOrigin(data.origin.x, data.origin.y);
    this.fires = data.fires || new Fires(this.scene);
    this.timer = this.scene.time.addEvent({
      delay: data.bullet.delay,
      callback: this._fire,
      callbackScope: this,
      loop: true,
    });
    this.bullet = data.bullet;
  }

  private _fire() {
    this.fires.createFire(this);
  }

  public isDead(): boolean {
    return this.x < -this.width;
  }

  public reset(): void {
    const data = Enemy.generateAttributes();
    super.reset(data.x, data.y);
    this.setFrame(data.frame);
  }
}
