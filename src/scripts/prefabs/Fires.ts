import { GameScene } from '../scenes/GameScene';
import { Enemy } from './Enemy';
import { Fire } from './Fire';
import { Player } from './Player';

export class Fires extends Phaser.Physics.Arcade.Group {
  constructor(private _scene: GameScene) {
    super(_scene.physics.world, _scene);
  }

  public createFire(source: Player): void {
    let fire: Fire = this.getFirstDead();
    if (!fire) {
      fire = Fire.generate(this._scene, source);
      this.add(fire);
    } else {
      fire.reset(source.x, source.y);
    }
    fire.move();
  }
}
