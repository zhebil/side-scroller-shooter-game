import { GameScene } from '../scenes/GameScene';
import { bullet } from './Enemy';
import { Fires } from './Fires';

export interface movableConstructorData {
  scene: GameScene;
  x: number;
  y: number;
  texture: string;
  velocity: number;
  bullet?: bullet;
  frame?: string;
  origin?: { x: number; y: number };
  fires?: Fires;
}
export class MovableObject extends Phaser.GameObjects.Sprite {
  scene: GameScene;
  body: Phaser.Physics.Arcade.Body;
  velocity: number;
  timer: Phaser.Time.TimerEvent;

  constructor(data: movableConstructorData) {
    const { scene, x, y, texture, frame } = data;
    super(scene, x, y, texture, frame);
    this.init(data);
  }

  public init(data: movableConstructorData): void {
    this.scene.add.existing(this);
    this.scene.physics.add.existing(this);
    this.body.enable = true;
    this.velocity = data.velocity;
    this.scene.events.on('update', this._update, this);
  }
  public reset(x: number, y: number): void {
    this.x = x;
    this.y = y;
    this.setAlive(true);
  }

  public isDead(): boolean {
    return false;
  }

  private _update(): void {
    if (this.active && this.isDead()) {
      this.setAlive(false);
    }
  }

  public move(): void {
    this.body.setVelocityX(this.velocity);
  }

  public setAlive(status: boolean): void {
    this.body.enable = status;
    this.setVisible(status);
    this.setActive(status);
    if (this.timer) {
      this.timer.paused = !status;
    }

    if (!status) {
      this.emit('killed');
    }
  }
}
