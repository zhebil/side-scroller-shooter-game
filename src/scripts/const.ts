export const ASSETS = '../../assets/';
const SpritesPath: string = ASSETS + 'sprites/';
export const SPRITES = {
  root: SpritesPath,
  BG: SpritesPath + 'background.png',
  dragon: {
    png: SpritesPath + 'dragon.png',
    json: SpritesPath + 'dragon.json',
  },
  enemy: {
    png: SpritesPath + 'enemy.png',
    json: SpritesPath + 'enemy.json',
  },
  boom: {
    png: SpritesPath + 'boom.png',
    json: SpritesPath + 'boom.json',
  },
  bullet: SpritesPath + 'bullet.png',
  fire: SpritesPath + 'fire.png',
};

export const WIDTH = 1280;
export const HEIGHT = 720;

export enum GameScenesName {
  Start = 'Start',
  Game = 'Game',
}
