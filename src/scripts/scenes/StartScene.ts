import { GameScenesName, HEIGHT, SPRITES, WIDTH } from '../const';
import { gameCompleteData } from './GameScene';

export enum fontStyles {
  font = '40px CurseCasual',
  color = '#ffffff',
}

export class StartScene extends Phaser.Scene {
  private _prevScore: number;

  constructor() {
    super(GameScenesName.Start);
  }

  public preload(): void {
    this.load.image('bg', SPRITES.BG);
  }

  public create(data: gameCompleteData): void {
    this._createBackground();
    if (data.score !== undefined) {
      this._createStats(data);
      this._prevScore = data.score;
    }
    this._createText();
    this._setEvents();
  }

  private _createStats(data: gameCompleteData): void {
    this.add
      .graphics()
      .fillStyle(0x000000, 0.5)
      .fillRoundedRect(WIDTH / 2 - 200, HEIGHT / 2 - 200, 400, 400);
    const textTitle = data.completed ? 'Level completed!' : 'Game Over';
    const textScore = `Score: ${data.score}`;
    const textStyle = {
      font: fontStyles.font,
      color: fontStyles.color,
    };
    this.add.text(WIDTH / 2, 250, textTitle, textStyle).setOrigin(0.5);
    this.add.text(WIDTH / 2, 350, textScore, textStyle).setOrigin(0.5);
  }

  private _createBackground(): void {
    this.add.sprite(0, 0, 'bg').setOrigin(0);
  }

  private _createText(): void {
    this.add
      .text(WIDTH / 2, 500, 'Tap to start', {
        font: fontStyles.font,
        color: fontStyles.color,
      })
      .setOrigin(0.5);
  }

  private _setEvents(): void {
    this.input.once('pointerdown', () => {
      this.scene.start(GameScenesName.Game, { prevScore: this._prevScore });
    });
  }
}
