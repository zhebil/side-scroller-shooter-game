import { GameScenesName, HEIGHT, WIDTH } from '../const';
import { Boom } from '../prefabs/Boom';
import { Enemies } from '../prefabs/Enemies';
import { MovableObject } from '../prefabs/MovableObject';
import { Player } from '../prefabs/Player';
import { fontStyles } from './StartScene';

export interface gameCompleteData {
  score: number;
  completed: boolean;
}

export interface sounds {
  boom: Phaser.Sound.BaseSound;
  theme: Phaser.Sound.BaseSound;
}
export class GameScene extends Phaser.Scene {
  private _player: Player;
  public cursors: Phaser.Types.Input.Keyboard.CursorKeys;
  private _bg: Phaser.GameObjects.TileSprite;
  private _enemies: Enemies;
  public score: number;
  private _scoreText: Phaser.GameObjects.Text;
  private _sounds: sounds;

  constructor() {
    super(GameScenesName.Game);
  }
  public init(): void {
    this.cursors = this.input.keyboard.createCursorKeys();
    this.score = 0;
  }

  public create(data: { prevScore: number }): void {
    this._createBackground();
    if (!this._sounds) {
      this._createSounds();
    }

    this._enemies = new Enemies(this, data.prevScore);

    this._player = new Player(this);
    this._addOverlap();

    this._createCompleteEvents();
    this._createText();
  }

  private _createSounds(): void {
    this._sounds = {
      boom: this.sound.add('boom', {
        volume: 0.1,
      }),
      theme: this.sound.add('theme', {
        loop: true,
        volume: 0.2,
      }),
    };
    this._sounds.theme.play();
  }

  private _createText(): void {
    this._scoreText = this.add.text(50, 50, 'Score: 0', {
      font: fontStyles.font,
      color: fontStyles.color,
    });
  }

  private _addOverlap(): void {
    this.physics.add.overlap(
      this._player.fires,
      this._enemies,
      this._onOverlap,
      undefined,
      this
    );
    this.physics.add.overlap(
      this._enemies.fires,
      this._player,
      this._onOverlap,
      undefined,
      this
    );
    this.physics.add.overlap(
      this._player,
      this._enemies,
      this._onOverlap,
      undefined,
      this
    );
  }

  private _createCompleteEvents(): void {
    this._player.once('killed', this.onComplete, this);
    this.events.on('enemies-killed', this.onComplete, this);
  }
  public onComplete(): void {
    this.scene.start('Start', {
      score: this.score,
      completed: this._player.active,
    });
  }

  private _onOverlap(source: MovableObject, target: MovableObject): void {
    const enemy = [source, target].find(i => i.texture.key === 'enemy');

    if (enemy) {
      ++this.score;
      this._scoreText.setText(`Score: ${this.score}`);
      Boom.generate(this, enemy.x, enemy.y);
      this._sounds.boom.play();
    }
    source.setAlive(false);
    target.setAlive(false);
  }

  public update(): void {
    this._player.move();
    this._bg.tilePositionX += 0.5;
  }

  private _createBackground(): void {
    this._bg = this.add.tileSprite(0, 0, WIDTH, HEIGHT, 'bg').setOrigin(0);
  }
}
