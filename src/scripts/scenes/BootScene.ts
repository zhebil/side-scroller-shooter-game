import { SPRITES } from '../const';

export class BootScene extends Phaser.Scene {
  constructor() {
    super('Boot');
  }
  public preload(): void {
    this.load.image('bg', SPRITES.BG);
  }
  public create(): void {
    this.scene.start('Preload');
  }
}
