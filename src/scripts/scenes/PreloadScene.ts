import { LoadingBar } from '../classes/LoadingBar';
import { SPRITES } from '../const';

export class PreloadScene extends Phaser.Scene {
  constructor() {
    super('Preload');
  }
  public preload(): void {
    this.add.sprite(0, 0, 'bg').setOrigin(0);
    const loadingBar = new LoadingBar(this);
    this._preloadAssets();
  }
  private _preloadAssets(): void {
    this.load.image('bullet', SPRITES.bullet);
    this.load.image('fire', SPRITES.fire);
    this.load.atlas('dragon', SPRITES.dragon.png, SPRITES.dragon.json);
    this.load.atlas('enemy', SPRITES.enemy.png, SPRITES.enemy.json);
    this.load.atlas('boom', SPRITES.boom.png, SPRITES.boom.json);

    this.load.audio('theme', 'assets/sounds/theme.mp3');
    this.load.audio('boom', 'assets/sounds/boom.mp3');
  }
  public create(): void {
    this.scene.start('Start');
  }
}
