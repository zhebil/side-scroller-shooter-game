import { HEIGHT, WIDTH } from '../const';
import { PreloadScene } from '../scenes/PreloadScene';

export class LoadingBar {
  private scene: PreloadScene;
  private style: any;
  private progressBar: Phaser.GameObjects.Graphics;
  private progressBox: Phaser.GameObjects.Graphics;

  constructor(scene: PreloadScene) {
    this.scene = scene;
    this.style = {
      boxColor: 0xd3d3d3,
      barColor: 0xfff8dc,
      x: WIDTH / 2 - 450,
      y: HEIGHT / 2 + 250,
      width: 900,
      height: 25,
    };

    this.progressBox = this.scene.add.graphics();
    this.progressBar = this.scene.add.graphics();

    this._showProgressBox();
    this._setEvents();
  }

  private _setEvents(): void {
    this.scene.load.on('progress', this._showProgressBar, this);
    this.scene.load.on('complete', this._onLoadComplete, this);
  }

  private _showProgressBox(): void {
    this.progressBox
      .fillStyle(this.style.boxColor)
      .fillRect(
        this.style.x,
        this.style.y,
        this.style.width,
        this.style.height
      );
  }

  private _onLoadComplete(): void {
    this.progressBar.destroy();
    this.progressBox.destroy();
  }

  private _showProgressBar(value: number): void {
    this.progressBar
      .clear()
      .fillStyle(this.style.barColor)
      .fillRect(
        this.style.x,
        this.style.y,
        this.style.width * value,
        this.style.height
      );
  }
}
