const { merge } = require('webpack-merge');
const common = require('./webpack.common');
var path = require('path');

const dev = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    hot: true,
    hotOnly: true,
  },
};

module.exports = merge(common, dev);
